import unittest
from .detection import Detection, PointDetection
from .detection import ShapeDetection, BoxShapeDetection


class DetectionTestCase(unittest.TestCase):
    def test_init(self):
        det = Detection(x_min=-1, y_min=-2,
                        x_max=1, y_max=2,
                        confidence=0.5)
        self.assertEqual(det.x_min, -1)
        self.assertEqual(det.y_min, -2)
        self.assertEqual(det.x_max, 1)
        self.assertEqual(det.y_max, 2)
        self.assertEqual(det.confidence, 0.5)

    def test_ints(self):
        det = Detection(x_min=-1.1, y_min=-2.1,
                        x_max=1.1, y_max=2.1)
        self.assertEqual(det.x_min, -1)
        self.assertEqual(det.y_min, -2)
        self.assertEqual(det.x_max, 1)
        self.assertEqual(det.y_max, 2)

    def test_setters(self):
        det = Detection(x_min=-2, y_min=-3,
                        x_max=2, y_max=3,
                        confidence=0.5)

        det.x_min = -1
        det.y_min = -2
        det.x_max = 1
        det.y_max = 2
        det.confidence = 1.0
        self.assertEqual(det.x_min, -1)
        self.assertEqual(det.y_min, -2)
        self.assertEqual(det.x_max, 1)
        self.assertEqual(det.y_max, 2)
        self.assertEqual(det.confidence, 1.0)


class PointDetectionTestCase(unittest.TestCase):
    def test_init(self):
        det = PointDetection(x=-1, y=-2,
                             confidence=0.5)
        self.assertEqual(det.x, -1)
        self.assertEqual(det.y, -2)
        self.assertEqual(det.confidence, 0.5)

    def test_setters(self):
        det = PointDetection(x=-2, y=-3,
                             confidence=0.5)
        det.x = -1
        det.y = -4
        det.confidence = 1.0
        self.assertEqual(det.x, -1)
        self.assertEqual(det.y, -4)
        self.assertEqual(det.confidence, 1.0)


class ShapeDetectionTestCase(unittest.TestCase):
    def test_init(self):
        det = ShapeDetection(xs=[1, 2, 3],
                             ys=[-1, -2, -3],
                             confidences=[.5, .6, .7])
        self.assertEqual(det.x_min, 1)
        self.assertEqual(det.y_min, -3)
        self.assertEqual(det.x_max, 3)
        self.assertEqual(det.y_max, -1)

        self.assertEqual(det.points[0].x, 1)
        self.assertEqual(det.points[0].y, -1)
        self.assertEqual(det.points[0].confidence, .5)

        self.assertEqual(det.points[1].x, 2)
        self.assertEqual(det.points[1].y, -2)
        self.assertEqual(det.points[1].confidence, .6)

        self.assertEqual(det.points[2].x, 3)
        self.assertEqual(det.points[2].y, -3)
        self.assertEqual(det.points[2].confidence, .7)


class BoxShapeDetectionTestCase(unittest.TestCase):
    def test_init(self):
        det = BoxShapeDetection(box_x_min=-5, box_y_min=-2,
                                box_x_max=5, box_y_max=2,
                                box_confidence=.5,
                                points_x=[-6, 0, 2],
                                points_y=[1, -3, -1],
                                points_confidence=[.1, .2, .3])

        self.assertEqual(det.x_min, -6)
        self.assertEqual(det.y_min, -3)
        self.assertEqual(det.x_max, 5)
        self.assertEqual(det.y_max, 2)

        self.assertEqual(det.points.points[0].x, -6)
        self.assertEqual(det.points.points[0].y, 1)
        self.assertEqual(det.points.points[0].confidence, .1)

        self.assertEqual(det.points.points[1].x, 0)
        self.assertEqual(det.points.points[1].y, -3)
        self.assertEqual(det.points.points[1].confidence, .2)

        self.assertEqual(det.points.points[2].x, 2)
        self.assertEqual(det.points.points[2].y, -1)
        self.assertEqual(det.points.points[2].confidence, .3)

        self.assertEqual(det.box.x_min, -5)
        self.assertEqual(det.box.y_min, -2)
        self.assertEqual(det.box.x_max, 5)
        self.assertEqual(det.box.y_max, 2)

    def test_x_min_setter(self):
        det = BoxShapeDetection(box_x_min=-2, box_y_min=-2,
                                box_x_max=2, box_y_max=2,
                                box_confidence=.5,
                                points_x=[-1, 1],
                                points_y=[-1, 1],
                                points_confidence=[.5, .5])
        det.x_min = -3
        self.assertEqual(det.x_min, -3)
        self.assertEqual(det.y_min, -2)
        self.assertEqual(det.x_max, 1)
        self.assertEqual(det.y_max, 2)

        self.assertEqual(det.box.x_min, -3)
        self.assertEqual(det.box.y_min, -2)
        self.assertEqual(det.box.x_max, 1)
        self.assertEqual(det.box.y_max, 2)

        self.assertEqual(det.points.points[0].x, -2)
        self.assertEqual(det.points.points[0].y, -1)
        self.assertEqual(det.points.points[1].x, 0)
        self.assertEqual(det.points.points[1].y, 1)

    def test_y_min_setter(self):
        det = BoxShapeDetection(box_x_min=-2, box_y_min=-2,
                                box_x_max=2, box_y_max=2,
                                box_confidence=.5,
                                points_x=[-1, 1],
                                points_y=[-1, 1],
                                points_confidence=[.5, .5])
        det.y_min = -3
        self.assertEqual(det.x_min, -2)
        self.assertEqual(det.y_min, -3)
        self.assertEqual(det.x_max, 2)
        self.assertEqual(det.y_max, 1)

        self.assertEqual(det.box.x_min, -2)
        self.assertEqual(det.box.y_min, -3)
        self.assertEqual(det.box.x_max, 2)
        self.assertEqual(det.box.y_max, 1)

        self.assertEqual(det.points.points[0].x, -1)
        self.assertEqual(det.points.points[0].y, -2)
        self.assertEqual(det.points.points[1].x, 1)
        self.assertEqual(det.points.points[1].y, 0)

    def test_x_max_setter(self):
        det = BoxShapeDetection(box_x_min=-2, box_y_min=-2,
                                box_x_max=2, box_y_max=2,
                                box_confidence=.5,
                                points_x=[-1, 1],
                                points_y=[-1, 1],
                                points_confidence=[.5, .5])
        det.x_max = 3
        self.assertEqual(det.x_min, -1)
        self.assertEqual(det.y_min, -2)
        self.assertEqual(det.x_max, 3)
        self.assertEqual(det.y_max, 2)

        self.assertEqual(det.box.x_min, -1)
        self.assertEqual(det.box.y_min, -2)
        self.assertEqual(det.box.x_max, 3)
        self.assertEqual(det.box.y_max, 2)

        self.assertEqual(det.points.points[0].x, 0)
        self.assertEqual(det.points.points[0].y, -1)
        self.assertEqual(det.points.points[1].x, 2)
        self.assertEqual(det.points.points[1].y, 1)

    def test_y_max_setter(self):
        det = BoxShapeDetection(box_x_min=-2, box_y_min=-2,
                                box_x_max=2, box_y_max=2,
                                box_confidence=.5,
                                points_x=[-1, 1],
                                points_y=[-1, 1],
                                points_confidence=[.5, .5])
        det.y_max = 3
        self.assertEqual(det.x_min, -2)
        self.assertEqual(det.y_min, -1)
        self.assertEqual(det.x_max, 2)
        self.assertEqual(det.y_max, 3)

        self.assertEqual(det.box.x_min, -2)
        self.assertEqual(det.box.y_min, -1)
        self.assertEqual(det.box.x_max, 2)
        self.assertEqual(det.box.y_max, 3)

        self.assertEqual(det.points.points[0].x, -1)
        self.assertEqual(det.points.points[0].y, 0)
        self.assertEqual(det.points.points[1].x, 1)
        self.assertEqual(det.points.points[1].y, 2)


if __name__ == '__main__':
    unittest.main()
