from typing import List
from .detection import Detection
from .detection import ShapeDetection, BoxShapeDetection
import numpy as np
from skimage.io import imread
from tqdm import tqdm


class Detector:
    def __init__(self, imgs: List[str]):
        """
        :param List[str] imgs: the image paths
        """
        self.imgs = imgs

    def _get_img_detections(self, im: np.ndarray) -> List[Detection]:
        """Return detected regions (e.g. faces) in the image.

        :param np.ndarray im: the image as a numpy array.
        :returns: a list of detections.
        :rtype: List[Detection]
        :raises NotImplementedError: if this method is not overridden
        """
        raise NotImplementedError

    def __iter__(self):
        for img in self.imgs:
            im = imread(img)
            yield self._get_img_detections(im)

    def __len__(self):
        return len(self.imgs)

    def get_detections(self) -> List[List[Detection]]:
        return [dets for dets in tqdm(self)]


class BoxDetector(Detector):
    """A detector with rectangular detections."""
    def _get_img_detections(self, img: np.ndarray)->List[Detection]:
        """Return detected rectangular regions in the image.

        :param np.ndarray img: the image as a numpy array.
        :returns: a list of box detections.
        :rtype: List[BoxDetection]
        :raises NotImplementedError: if this method is not overridden
        """
        raise NotImplementedError


class LandmarkDetector(Detector):
    """A detector where a detection contains multiple points.

    A typical example of a point detector is a detector that marks facial
    landmarks, or body landmarks (e.g. Dlib's face shape predictor)
    """

    def _get_img_detections(self, img: np.ndarray)->List[ShapeDetection]:
        """Return detected point collections in the image.

        :param np.ndarray img: the image as a numpy array.
        :returns: a list of point detections.
        :rtype: List[PointsDetection]
        :raises NotImplementedError: if this method is not overridden
        """
        raise NotImplementedError


class BoxLmDetector(Detector):
    """A detector with rectangular and landmark detecions."""
    def _get_img_detections(self, img: np.ndarray)->List[BoxShapeDetection]:
        """Return detected rectangular regions in the image.

        :param np.ndarray img: the image as a numpy array.
        :returns: a list of box detections.
        :rtype: List[BoxDetection]
        :raises NotImplementedError: if this method is not overridden
        """
        raise NotImplementedError
