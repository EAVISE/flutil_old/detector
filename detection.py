from ..shape import Point, Box, Shape
from typing import List
from matplotlib.patches import Circle
from matplotlib.patches import Patch


class Detection(Box):
    def __init__(self,
                 x_min: float, y_min: float,
                 x_max: float, y_max: float,
                 confidence: float = None):
        super().__init__(int(x_min), int(y_min),
                         int(x_max), int(y_max))
        self.confidence = confidence

    @property
    def confidence(self) -> float:
        """Confidence score of the detection."""
        return self._confidence

    @confidence.setter
    def confidence(self, value):
        self._confidence = value

    @property
    def x_min(self):
        return int(super().x_min)

    @x_min.setter
    def x_min(self, value):
        Box.x_min.fset(self, int(value))

    @property
    def y_min(self):
        return int(super().y_min)

    @y_min.setter
    def y_min(self, value):
        Box.y_min.fset(self, int(value))

    @property
    def x_max(self):
        return int(super().x_max)

    @x_max.setter
    def x_max(self, value):
        Box.x_max.fset(self, int(value))

    @property
    def y_max(self):
        return int(super().y_max)

    @y_max.setter
    def y_max(self, value):
        Box.y_max.fset(self, int(value))

    def to_patches(self, *args, **kwargs) -> List[Patch]:
        return [self.to_patch(*args, **kwargs)]


class PointDetection(Point, Detection):
    def __init__(self, x: float, y: float, confidence: float = None):
        Detection.__init__(self, x, y, x, y, confidence)
        Point.__init__(self, x, y)

    @property
    def x(self):
        assert self.x_min == self.x_max
        return self.x_min

    @x.setter
    def x(self, value):
        self.x_min = value
        self.x_max = value

    @property
    def y(self):
        assert self.y_min == self.y_max
        return self.y_min

    @y.setter
    def y(self, value):
        self.y_min = value
        self.y_max = value

    def to_patches(self, **kwargs) -> List[Patch]:
        if 'alpha' in kwargs:
            alpha = kwargs['alpha']
            del kwargs['alpha']
        elif self.confidence is not None:
            alpha = self.confidence
        else:
            alpha = 0.3

        if 'radius' in kwargs:
            radius = kwargs['radius']
            del kwargs['radius']
        else:
            radius = 5

        if 'color' in kwargs:
            color = kwargs['color']
            del kwargs['color']
        else:
            color = 'white'

        return [Circle(tuple(self.center),
                       alpha=alpha,
                       radius=radius,
                       color=color,
                       **kwargs)]


class ShapeDetection(Shape, Detection):
    """A detection consisting of a set of points."""
    def __init__(self, xs, ys, confidences: List[float] = None):

        if len(xs) != len(ys):
            raise ValueError('xs and ys must be of the same length.')

        if confidences is not None:
            points = [PointDetection(x, y, c)
                      for x, y, c in zip(xs, ys, confidences)]
        else:
            points = [PointDetection(x, y)
                      for x, y in zip(xs, ys)]

        super().__init__(points)

    def to_patches(self, **kwargs) -> List[Patch]:
        return [patch
                for p in self.points
                for patch in p.to_patches(**kwargs)]


class BoxShapeDetection(Detection):
    """A detection consisting of a box and a set of points."""
    def __init__(self,
                 box_x_min, box_y_min,
                 box_x_max, box_y_max,
                 points_x, points_y,
                 box_confidence: List[float] = None,
                 points_confidence: List[float] = None):
        if len(points_x) != len(points_y):
            raise ValueError('points_x and points_y must be '
                             'of the same length.')

        self.points = ShapeDetection(points_x, points_y, points_confidence)
        self.box = Detection(box_x_min, box_y_min,
                             box_x_max, box_y_max,
                             box_confidence)

        self.confidence = box_confidence

        super().__init__(self.x_min, self.y_min, self.x_max, self.y_max)

    def to_patches(self, **kwargs) -> List[Patch]:
        """Return list of patches that represent the detection.

        The kwargs should be prefixed with "box_..." or "point_..." to indicate
        whether the kwarg is meant for the box patch or the point patches. If
        no prefix is given, the kwarg will be passed to both.
        """
        box_kwargs = {k.replace('box_', ''): v
                      for k, v in kwargs.items()
                      if not k.startswith('point_')}
        points_kwargs = {k.replace('point_', ''): v
                         for k, v in kwargs.items()
                         if not k.startswith('box_')}

        return [*self.box.to_patches(**box_kwargs),
                *self.points.to_patches(**points_kwargs)]

    @property
    def x_min(self):
        return min(self.points.x_min, self.box.x_min)

    @x_min.setter
    def x_min(self, value):
        if self.box.x_min < self.points.x_min:
            diff = self.points.x_min - self.box.x_min
            self.box.x_min = value
            self.points.x_min = self.box.x_min + diff
        else:
            diff = self.box.x_min - self.points.x_min
            self.points.x_min = value
            self.box.x_min = self.points.x_min + diff

    @property
    def y_min(self):
        return min(self.points.y_min, self.box.y_min)

    @y_min.setter
    def y_min(self, value):
        if self.box.y_min < self.points.y_min:
            diff = self.points.y_min - self.box.y_min
            self.box.y_min = value
            self.points.y_min = self.box.y_min + diff
        else:
            diff = self.box.y_min - self.points.y_min
            self.points.y_min = value
            self.box.y_min = self.points.y_min + diff

    @property
    def x_max(self):
        return max(self.points.x_max, self.box.x_max)

    @x_max.setter
    def x_max(self, value):
        if self.box.x_max > self.points.x_max:
            diff = self.points.x_max - self.box.x_max
            self.box.x_max = value
            self.points.x_max = self.box.x_max + diff
        else:
            diff = self.box.x_max - self.points.x_max
            self.points.x_max = value
            self.box.x_max = self.points.x_max + diff

    @property
    def y_max(self):
        return max(self.points.y_max, self.box.y_max)

    @y_max.setter
    def y_max(self, value):
        if self.box.y_max > self.points.y_max:
            diff = self.points.y_max - self.box.y_max
            self.box.y_max = value
            self.points.y_max = self.box.y_max + diff
        else:
            diff = self.box.y_max - self.points.y_max
            self.points.y_max = value
            self.box.y_max = self.points.y_max + diff
