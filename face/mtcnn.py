import logging
from typing import List
import tensorflow as tf
import numpy as np
from .. import BoxLmDetector
from ..detection import BoxShapeDetection
from ..detection import Detection, ShapeDetection
from .etc.align import detect_face


class MTCNN(BoxLmDetector):
    def __init__(self,
                 imgs,
                 det_minsize=50,
                 det_threshold=[0.4, 0.6, 0.6],
                 det_factor=0.9):
        super().__init__(imgs)
        with tf.Graph().as_default():
            config = tf.ConfigProto()
            config.gpu_options.per_process_gpu_memory_fraction = 0.2
            sess = tf.Session(config=config)

            with sess.as_default():
                pnet, rnet, onet = detect_face.create_mtcnn(sess, None)

        self.pnet = pnet
        self.rnet = rnet
        self.onet = onet
        self.det_minsize = det_minsize
        self.det_threshold = det_threshold
        self.det_factor = det_factor

    def _get_img_detections(self, img: np.ndarray)->List[Detection]:
        if img.ndim != 3:
            logging.warning('Image has ndim != 3')
            return []

        raw_boxes, raw_points = detect_face.detect_face(img,
                                                        self.det_minsize,
                                                        self.pnet,
                                                        self.rnet,
                                                        self.onet,
                                                        self.det_threshold,
                                                        self.det_factor)
        if len(raw_boxes) != 0 and len(raw_points) != 0:
            points = [p for p in zip(raw_points[:5].T, raw_points[5:].T)]
            return [BoxShapeDetection(box_x_min=b[0],
                                      box_y_min=b[1],
                                      box_x_max=b[2],
                                      box_y_max=b[3],
                                      box_confidence=b[4],
                                      points_x=p[0],
                                      points_y=p[1])
                    for b, p in zip(raw_boxes, points)]
        elif len(raw_boxes) != 0:
            return [Detection(x_min, y_min, x_max, y_max, conf)
                    for (x_min, y_min, x_max, y_max, conf) in raw_boxes]
        elif len(raw_points) != 0:
            return [ShapeDetection(xs=face_xs, ys=face_ys)
                    for face_xs, face_ys in zip(raw_points[:5].T,
                                                raw_points[5:].T)]
        else:
            return []
