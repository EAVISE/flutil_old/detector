from ..detector import LandmarkDetector
from ..detection import ShapeDetection
import numpy as np
from typing import List


class OpenPoseFaceDetector(LandmarkDetector):
    def _get_img_detections(self, img: np.array) -> List[ShapeDetection]:
        raise NotImplementedError()
