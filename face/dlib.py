from ..detector import Detector, LandmarkDetector
from ..detection import Detection, ShapeDetection
import dlib
import numpy as np
from ...shape import Box
from typing import List
from pathlib import Path


FACE_MODEL = str(Path(__file__).parent / 'etc/mmod_human_face_detector.dat')
SHAPE_PRED_PATH = str(Path(__file__).parent
                      / 'etc/shape_predictor_5_face_landmarks.dat')


class CnnDetector(Detector):
    """DLIB CNN implementation of a face detector."""

    def __init__(self, imgs):
        super().__init__(imgs)
        self._detector = dlib.cnn_face_detection_model_v1(FACE_MODEL)

    def _get_img_detections(self, img: np.array)->List[Box]:
        faces = self._detector(img, 1)
        return [Detection(x_min=face.rect.left(),
                          x_max=face.rect.right(),
                          y_min=face.rect.top(),
                          y_max=face.rect.bottom())
                for face in faces]


class FrontDetector(Detector):
    """DLIB implementation of a frontal face detector."""

    def __init__(self, imgs):
        super().__init__(imgs)
        self.__detector = dlib.get_frontal_face_detector()

    def _get_img_detections(self, img: np.array)->List[Box]:
        faces = self.__detector(img, 1)
        return [Detection(x_min=face.left(),
                          x_max=face.right(),
                          y_min=face.top(),
                          y_max=face.bottom())
                for face in faces]


class FacePointsDetector(LandmarkDetector):
    """DLIB implementation of a facial landmark detector."""

    def __init__(self, imgs):
        super().__init__(imgs)
        self._shape_pred = dlib.shape_predictor(SHAPE_PRED_PATH)
        self._detector = CnnDetector()

    def _get_img_detections(self, img: np.array) -> List[ShapeDetection]:
        dets = self._detector._get_img_detections(img)
        detections = []
        for det in dets:
            dlibdet = dlib.rectangle(left=det.left, right=det.right,
                                     top=det.top, bottom=det.bottom)
            shape = self._shape_pred(img, dlibdet)
            detections.append([ShapeDetection(p.x, p.y)
                               for p in shape.parts()])
        return detections
