from face_recognition import api
from ..detector import BoxDetector
from ..detection import Detection
import numpy as np
from typing import List


class AgeitFaceDetector(BoxDetector):
    def _get_img_detections(self, img: np.ndarray)->List[Detection]:
        locs = api.face_locations(img)
        # Locations are given in CSS order (top, right, bottom, left)
        return [Detection(x_min=loc[3],  # left
                          x_max=loc[1],  # right
                          y_min=loc[0],  # top
                          y_max=loc[2])  # bottom
                for loc in locs]
